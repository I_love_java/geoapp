package clientgeohttp.geoservice;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.os.AsyncTask;
import android.widget.Toast;
import java.io.IOException;

/**
 * Created by hobo on 05.03.2015.
 */
public class AsyncGETUnloader extends AsyncTask<String, Void, String> {

    Context context;
    DBHelper dbHelper = new DBHelper(context, "LatLonTimeDB.db", null, 1);
    String[] resultCol;
    String URL;
    Cursor cursor;
    Integer cursorCount;

    //Constructor init vars
    AsyncGETUnloader(Context context){
        super();
        this.context=context;
    }

    //Write URL tp DB, try unload DB
    @Override
    protected String doInBackground(String... urls) {
        /*
         * FIRST STEP - write to DB url
         */
        writeUrlToDB(urls[0]);
        /*
         * STEP TWO - try to send GET and unload DB
         */
        tryToSendGetAndClearDB();

        //ger cursor count for return
        cursorCount = cursor.getCount();

        //close cursor and db
        cursor.close();
        dbHelper.close();

        return cursorCount.toString();
    }

    void writeUrlToDB(String URL){
        //Create map for write URL to DB
        ContentValues newValues = new ContentValues();
        newValues.put("url", URL);

        //Write map to db
        dbHelper.getWritableDatabase().insert("TableURL", null, newValues);

        //Show text and clear
        Toast.makeText(context, "Input to DB - OK", Toast.LENGTH_LONG).show();
        newValues.clear();
    }
    void tryToSendGetAndClearDB(){
         /*
         * STEP TWO
         */
        //Init cursor for unload URLS from DB
        resultCol = new String[]{"_id", "url"};
        cursor = dbHelper.getWritableDatabase().query("TableURL", resultCol, null, null, null, null, null);

        //unload DB and send GET on server
        try {
            while (cursor.moveToNext() & cursor.getCount() != -1) {
                 URL = cursor.getString(1);
                //try to send get
                new GetHelper().GET(URL);
                //if no exception delete URL from DB
                dbHelper.getWritableDatabase().delete("TableURL", "_id == " + cursor.getString(0), null);
            }}
        catch (IOException e) {e.printStackTrace();}

        //Return number of strings DB
        cursor = dbHelper.getWritableDatabase().query("TableURL", resultCol, null, null, null, null, null);
    }

    @Override
    protected void onPostExecute(String result) { 
        //Print number of strings in DB
        Toast.makeText(context, "Strings in db" + result, Toast.LENGTH_LONG).show(); }
}
