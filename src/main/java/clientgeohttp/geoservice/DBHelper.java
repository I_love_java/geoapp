package clientgeohttp.geoservice;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by hobo on 04.03.2015.
 */
public class DBHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "urlDB.db";
    private static final String DATABASE_TABLE = "TableURL";
    private static final int DATABASE_VERSION = 1;

    private static final String DATABASE_CREATE = "create table " + DATABASE_TABLE +
            " (_id INTEGER PRIMARY KEY AUTOINCREMENT, url TEXT)";

    public DBHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(DATABASE_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    db.execSQL("DROP TABLE IF IT EXISTS "+ DATABASE_TABLE);
    onCreate(db);
    }
}
