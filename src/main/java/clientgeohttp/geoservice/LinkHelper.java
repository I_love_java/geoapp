package clientgeohttp.geoservice;

import android.app.Activity;

/**
 * Created by hobo on 08.04.2015.
 */
public class LinkHelper extends Activity {
    //input url authority and path
    private String a_URL;
    //Array for names vars
    private String [] vars;
    //url  with args on out
    private String created_URL;

    public LinkHelper() {
         //initialize marks from strings.xml
         vars = getResources().getStringArray(R.array.URL_vars);
         //initialize url,
         a_URL = getResources().getString(R.string.a_URL);
    }

    //init var in the Link and return it
    public String getHttpUrlGet(String ... args) {
    //add the ? symbol for http protocol
        created_URL = a_URL + "?";
    //before the first variable symbol & not need
        vars[0]+=args[0];
    //init all next vars
        for (int i=1; i<args.length; i++){
            vars[i]+=("&" +args[i]);
        }

        return created_URL;
    }
}