package clientgeohttp.geoservice;

/**
 * Created by hobo on 02.03.2015.
 */

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Looper;
import android.os.SystemClock;
import android.telephony.TelephonyManager;
import android.widget.Toast;


import java.text.SimpleDateFormat;
import java.util.Date;


public class GeoService extends Service {

    private AlarmManager alarmManager;
    private PendingIntent alarmIntent;
    private SimpleDateFormat date_format;
    private SimpleDateFormat time_format;
    private TelephonyManager telephonyManager;
    private String START_GEO_SERVICE;
    private Intent intent;
    private String deviceId;
    private long current_time;
    private long start_after;

    @Override
    public void onCreate() {
        super.onCreate();
        initVariables();
    }

    void initVariables(){
        //init date/time formaters
        date_format = new SimpleDateFormat("MM/dd/yy");
        time_format = new SimpleDateFormat("HH:mm:ss");

       //getting device ID
        telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        deviceId = telephonyManager.getDeviceId().toString();
        /*
        *Create intent and set him to Alarm intent how broadcast
        *in the future Alarm Manager get it and run broadcast intent
        */
        START_GEO_SERVICE = "com.GeoService.action.START_GEO_SERVICE";
        intent = new Intent(START_GEO_SERVICE);
        alarmIntent = PendingIntent.getBroadcast(this, 0, intent, 0);
        //Create Alarm Manager for schedule intent
        alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startID) {

    //Set alarm manager next time for run service, old alarm will be delete from memory
    int getStart_after_ms = Integer.parseInt(getResources().getString(R.string.Start_after_ms));
    current_time = SystemClock.elapsedRealtime();
    start_after =  SystemClock.elapsedRealtime() + getStart_after_ms;
    alarmManager.setInexactRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, start_after,current_time , alarmIntent);
    //start the thread
    service_thread.start();

        /* that, after returning from onStartCreated(), if the process is killed with no remaining
         *start commands to deliver, then the service will be stopped instead of restarted.
         */
        return Service.START_NOT_STICKY;
    }


    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    Thread service_thread = new Thread(new Runnable() {
        @Override
        public void run() {

            LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            Criteria criteria = new Criteria();
            criteria.setAccuracy(Criteria.ACCURACY_MEDIUM);
            criteria.setPowerRequirement(Criteria.POWER_LOW);
            criteria.setBearingRequired(false);
            criteria.setSpeedRequired(false);
            criteria.setAltitudeRequired(false);
            criteria.setCostAllowed(false);

            locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
            locationManager.requestSingleUpdate(criteria, locationListener, Looper.getMainLooper());
            stopSelf();
        }
    });

    LocationListener locationListener = new LocationListener() {

        @Override
        public void onLocationChanged(Location location) {

            //create vars and init it
            String date = date_format.format(new Date(location.getTime()));
            String time = time_format.format(new Date(location.getTime()));
            String date_time = date + "%20" + time;
            String lat = ""+location.getLatitude();
            String lon = ""+location.getLongitude();

            //Create link helper and get link
            LinkHelper linkHelper = new LinkHelper();
            String URL = linkHelper.getHttpUrlGet(deviceId, date_time, lat, lon);

            new AsyncGETUnloader(getBaseContext()).execute(URL);

            Toast.makeText(getBaseContext(), "Lat " + location.getLatitude() + " Lon " + location.getLongitude(), Toast.LENGTH_LONG).show();
        }

        @Override
        public void onProviderDisabled(String provider) {}
        @Override
        public void onProviderEnabled(String provider) {}
        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {}
    };

}